from core import LspCore
import click


@click.command()
@click.argument("path", type=click.Path(exists=True, file_okay=False))
@click.option("-a", is_flag=True, help="Shows all files")
@click.option("-dirs", is_flag=True, help="Show only dirs")
def start(path, a, dirs):
    LspCore(path, a, dirs).start()
