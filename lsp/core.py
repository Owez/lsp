import os
import sys


class PrintColours:
    """
    Unicode and colour codes for using in printing
    """

    U_FOLDER = " 🗀"
    U_FILE = " 🗎"


class LspCore:
    """
    The core class of lsp
    """

    def __init__(self, path, should_show_all, dir_only):
        self.all_files = self._get_all_files(path, dir_only)
        self.should_show_all = should_show_all

    def start(self):
        if self.should_show_all:
            final_files = self.all_files
        else:
            final_files = self._filter_hidden(self.all_files)

        self._display_output(final_files)

    def _get_all_files(self, path, dir_only):
        output = []

        for _, dirs, filenames in os.walk(path):
            if dir_only:
                output.append(dirs)
            else:
                output.extend([dirs, filenames])

            break

        return output

    def _filter_hidden(self, all_files):
        output = []

        for nested_key in all_files:
            output.extend([self._find_filtered(nested_key)])

        return output

    def _find_filtered(self, nested_files):
        return [key for key in nested_files if not key.startswith(".")]

    def _display_output(self, final_files):
        if not final_files:
            print("Nothing to display!")
            sys.exit(1)

        file_type_match = ["folder", "file"]
        print_colours = PrintColours()

        for file_type in range(len(final_files)):
            for file in final_files[file_type]:
                matched_filetype = file_type_match[file_type]
                formatted_file = self._format_file(file)
                permissions = ""

                if matched_filetype == "folder":
                    print(f"{print_colours.U_FOLDER}{formatted_file} {permissions}f") # folder unicode
                elif matched_filetype == "file":
                    print(f"{print_colours.U_FILE}{formatted_file} {permissions}") # folder unicode

    def _format_file(self, file):
        return self._format_file_spacing(self._format_file_len(file))

    def _format_file_len(self, file):
        splitted_file = file.split(".")

        if len(splitted_file) is 1:
            return splitted_file[0]

        return splitted_file[0][:10] + "." + splitted_file[-1][:10]

    def _format_file_spacing(self, file):
        output = "  ｜ "

        # NOTE if you want spaces @ the start
        # for letter in reversed(range(20)):
        #     if letter < len(file):
        #         output += file[-letter]
        #     else:
        #         output += " "

        for letter in range(20):
            if letter < len(file):
                output += file[letter]
            else:
                output += " "

        return output
