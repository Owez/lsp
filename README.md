# lsp

## About lsp

`lsp` is a small python program designed as a rewrite to the popular `ls` command found on all mainstream operating systems. Essentially, `lsp` is a simple utility written in python that intelligently lists all files/directories in a customizable way.

Please note that this project is not intended to replace anything long-term & is just a small hobby project, completed in 1.5 days.

## Installing

To install `lsp`, simply clone this repository with:

```bash
git clone https://gitlab.com/scOwez/lsp
```

Then you can run it with `python lsp` once you have cd'd into the cloned directory.

You may also want to add `lsp` into your `~/.bashrc` in Linux. If you would like to do this, please move the lsp module directory (the `lsp` directory next to `README.md`, not the cloned folder) into `/usr/bin` and append this to the bottom of `~/.bashrc`:

```bash
# `lsd` Package
alias ls='python3 lsd/'
alias l='python3 lsd/'
alias la='python3 lsd/ -a'
```

----

*scOwez, Licensed under* ***Apache 2.0***
